﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.IO;

//Definition of raw encoder data

//righthand.FFSensorS = hand.SensorReadings.RawDataADC[3];
//righthand.FFSensorB = hand.SensorReadings.RawDataADC[4];
//righthand.MFSensorS = hand.SensorReadings.RawDataADC[5];
//righthand.MFSensorB = hand.SensorReadings.RawDataADC[6];
//righthand.RFSensorS = hand.SensorReadings.RawDataADC[7];
//righthand.RFSensorB = hand.SensorReadings.RawDataADC[8];
//righthand.LFSensorS = hand.SensorReadings.RawDataADC[9];
//righthand.LFSensorB = hand.SensorReadings.RawDataADC[10];

//TODO: Definition of pose data

namespace DexmoController
{
    class Program
    {
        private volatile static int[] pose = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
        private static System.Object threadLock = new System.Object();

        private static uint FFSpreadMax = 2868;
        private const uint FFSpreadMin = 1956;
        private static uint MFSpreadMax = 2464;
        private static uint MFSpreadMin = 1481;
        private const uint RFSpreadMax = 2000;
        private static uint RFSpreadMin = 1500;
        private const uint LFSpreadMax = 2015;
        private static uint LFSpreadMin = 1210;
        private static uint THSpreadMax = 4000;
        private static uint THSpreadMin = 2300;

        private static uint THRMax = 1000;
        private static uint THRMin = 840;

        private volatile static bool running = true;

        private static double[] ffAlpha = new double[3000];
        private static double[] ffEncoder = new double[3000];
        private static double[] mfAlpha = new double[3000];
        private static double[] mfEncoder = new double[3000];
        private static double[] rfAlpha = new double[3000];
        private static double[] rfEncoder = new double[3000];
        private static double[] lfAlpha = new double[3000];
        private static double[] lfEncoder = new double[3000];
        private static double[] thAlpha = new double[3000];
        private static double[] thEncoder = new double[3000];

        static void Communication(Socket sender)
        {
            while (true)
            {
                if (!running)
                {
                    break;
                }

                string pose_string = "<HEAD> ";
                lock (threadLock)
                {
                    for(int i = 0; i < 11; i++)
                    {
                        pose_string = pose_string + pose[i].ToString() + " ";
                    }
                }
                pose_string = pose_string + "<EOF>";
                byte[] payload = Encoding.ASCII.GetBytes(pose_string);
                Console.WriteLine(pose_string);
                int byteSent = sender.Send(payload);

                // Receive the response from the remote device.  
                //int bytesRec = sender.Receive(bytes);
                //Console.WriteLine("Echoed test = {0}",
                //    Encoding.ASCII.GetString(bytes, 0, bytesRec));
            }
        }

        static void Pose(Libdexmo.Client.Controller controller)
        {
            int[] pose_buffer = new int[] { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };
            while (true)
            {
                if (!running)
                {
                    break;
                }

                var frame = controller.GetFrame();
                if(frame != null)
                {
                    //controller.ImpedanceControlOneFinger(
                    //       frame.Hand.DeviceAssignedId,
                    //       Libdexmo.Model.FingerType.Index,
                    //       0.5f,
                    //       0.2f,
                    //       true);

                    controller.StopImpedanceControlFingersAll(frame.Hand.DeviceAssignedId);

                    var hand = controller.LatestHands.First().Value;

                    uint FFSpread = hand.SensorReadings.RawDataADC[3] - FFSpreadMin;
                    uint MFSpread = hand.SensorReadings.RawDataADC[5] - MFSpreadMin;
                    uint RFSpread = RFSpreadMax - hand.SensorReadings.RawDataADC[7]; // - RFSpreadMax;
                    uint LFSpread = LFSpreadMax - hand.SensorReadings.RawDataADC[9]; // - LFSpreadMax;
                    uint THSpread = hand.SensorReadings.RawDataADC[1] - THSpreadMin;

                    uint THR = THRMax - hand.SensorReadings.RawDataADC[0];

                    pose_buffer[0] = 180 - (int)RadianToDegree(MatchAlpha(hand.SensorReadings.RawDataADC[4], ffEncoder, ffAlpha));
                    pose_buffer[1] = -(int)FFSpread * 20 / ((int)FFSpreadMax - (int)FFSpreadMin);

                    pose_buffer[2] = 180 - (int)RadianToDegree(MatchAlpha(hand.SensorReadings.RawDataADC[6], mfEncoder, mfAlpha));
                    pose_buffer[3] = -(int)MFSpread * 30 / ((int)MFSpreadMax - (int)MFSpreadMin) + 15;

                    pose_buffer[4] = 180 - (int)RadianToDegree(MatchAlpha(hand.SensorReadings.RawDataADC[8], rfEncoder, rfAlpha));
                    pose_buffer[5] = -(int)RFSpread * 20 / ((int)RFSpreadMax - (int)RFSpreadMin);

                    pose_buffer[6] = 180 - (int)RadianToDegree(MatchAlpha(hand.SensorReadings.RawDataADC[10], lfEncoder, lfAlpha));
                    pose_buffer[7] = -(int)LFSpread * 20 / ((int)LFSpreadMax - (int)LFSpreadMin);

                    pose_buffer[8] = 180 - (int)RadianToDegree(MatchAlpha(hand.SensorReadings.RawDataADC[2], thEncoder, thAlpha));

                    pose_buffer[9] = -(int)THSpread * 24 / ((int)THSpreadMax - (int)THSpreadMin) + 12;
                    pose_buffer[10] = (int)THR * 70 / ((int)THRMax - (int)THRMin);

                    lock (threadLock)
                    {
                        for(int i = 0; i < 11; i++) {
                            pose[i] = pose_buffer[i];
                        }
                    }
                }
            }
        }

        static double MatchAlpha(uint encoderValue, double[] encoderAngle, double[] alpha) {
            // Binary search for encoder angle matching the encoder value
            // https://msdn.microsoft.com/en-us/library/2cy9f6wb(v=vs.110).aspx
            int index = Array.BinarySearch(encoderAngle, EncoderToDegree(encoderValue));
            //Console.WriteLine(index);
            if(index < 0) {
                index = ~index - 1;
            }

            if(index < 0) {
                index = 0;
            }
            Console.Write(alpha[index]);
            Console.Write(" ");
            Console.Write(EncoderToDegree(encoderValue));
            Console.Write("\n");
            return alpha[index];
        }

        static double EncoderToDegree(uint encoderValue) {
            return (double)encoderValue / 3200 * 180;
        }

        static double RadianToDegree(double rad) {
            return rad / Math.PI * 180;
        }

        static double DegreeToRadian(double degree){
            return degree / 180 * Math.PI;
        }


        static void Main(string[] args)
        {
            //Hand lefthand = new Hand();
            //Hand righthand = new Hand();
            Libdexmo.Client.Controller controller = new Libdexmo.Client.Controller();

            IPAddress ipAddress = IPAddress.Parse("fe80::813b:1b99:a757:a426");
            IPEndPoint remoteEP = new IPEndPoint(ipAddress, 10000);
            Socket sender = new Socket(ipAddress.AddressFamily,
                SocketType.Stream, ProtocolType.Tcp);
            try {
                sender.Connect(remoteEP);
                Console.WriteLine("Socket connected to {0}", sender.RemoteEndPoint.ToString());
            } catch (ArgumentNullException ane) {
                Console.WriteLine("ArgumentNullException : {0}", ane.ToString());
            } catch (SocketException se) {
                Console.WriteLine("SocketException : {0}", se.ToString());
            } catch (Exception e) {
                Console.WriteLine("Unexpected exception : {0}", e.ToString());
            }


            //Read mapping from txt files
            Console.WriteLine("Reading mapping...");
           
            StreamReader reader = File.OpenText("ff.txt");
            string line;
            for(int i = 0; i < 3000; i++) {
                line = reader.ReadLine();
                string[] pair = line.Split(' ');
                ffEncoder[i] = RadianToDegree(Double.Parse(pair[0]));
                ffAlpha[i] = Double.Parse(pair[1]);
            }
            reader.Close();

            reader = File.OpenText("mf.txt");
            for (int i = 0; i < 3000; i++) {
                line = reader.ReadLine();
                string[] pair = line.Split(' ');
                mfEncoder[i] = RadianToDegree(Double.Parse(pair[0]));
                mfAlpha[i] = Double.Parse(pair[1]);
            }
            reader.Close();

            reader = File.OpenText("rf.txt");
            for (int i = 0; i < 3000; i++)
            {
                line = reader.ReadLine();
                string[] pair = line.Split(' ');
                rfEncoder[i] = RadianToDegree(Double.Parse(pair[0]));
                rfAlpha[i] = Double.Parse(pair[1]);
            }
            reader.Close();

            reader = File.OpenText("lf.txt");
            for (int i = 0; i < 3000; i++)
            {
                line = reader.ReadLine();
                string[] pair = line.Split(' ');
                lfEncoder[i] = RadianToDegree(Double.Parse(pair[0]));
                lfAlpha[i] = Double.Parse(pair[1]);
            }
            reader.Close();

            reader = File.OpenText("th.txt");
            for (int i = 0; i < 3000; i++) {
                line = reader.ReadLine();
                string[] pair = line.Split(' ');
                thEncoder[i] = RadianToDegree(Double.Parse(pair[0]));
                thAlpha[i] = Double.Parse(pair[1]);
            }
            reader.Close();

            Console.WriteLine("Reading mapping complete.");

            //Console.WriteLine("Calibrating FFSpreadMax");
            //Console.ReadKey();
            //var frame = controller.GetFrame();
            //var hand = controller.LatestHands.First().Value;
            //FFSpreadMax = hand.SensorReadings.RawDataADC[3];

            //Console.WriteLine("Calibrating MFSpreadMax");
            //Console.ReadKey();
            //frame = controller.GetFrame();
            //hand = controller.LatestHands.First().Value;
            //MFSpreadMax = hand.SensorReadings.RawDataADC[5];

            //Console.WriteLine("Calibrating MFSpreadMin");
            //Console.ReadKey();
            //frame = controller.GetFrame();
            //hand = controller.LatestHands.First().Value;
            //MFSpreadMin = hand.SensorReadings.RawDataADC[5];

            //Console.WriteLine("Calibrating RFSpreadMin");
            //Console.ReadKey();
            //frame = controller.GetFrame();
            //hand = controller.LatestHands.First().Value;
            //RFSpreadMin = hand.SensorReadings.RawDataADC[7];

            //Console.WriteLine("Calibrating LFSpreadMin");
            //Console.ReadKey();
            //frame = controller.GetFrame();
            //hand = controller.LatestHands.First().Value;
            //LFSpreadMin = hand.SensorReadings.RawDataADC[9];

            System.Threading.Thread commThread = new System.Threading.Thread(()=>Communication(sender));
            System.Threading.Thread poseThread = new System.Threading.Thread(() => Pose(controller));

            commThread.IsBackground = true;
            poseThread.IsBackground = true;

            commThread.Start();
            poseThread.Start();

            Console.ReadKey();
            running = false;
        }
    }
}
